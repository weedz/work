#include "stdAfx.h"

using namespace std;

void Init(vector<company> &comp)
{
	comp.clear();
	ifstream file(companyFile);
	string temp_str;
	int temp_int[4];
	double temp_double[1];
	vector<string> vstring;
	if (getFileSize(companyFile) > 0)
	{
		while(!file.eof())
		{
			getline(file, temp_str);
			explode(temp_str, " ", &vstring);
			if ( vstring.size() > 0)
			{
				if ( vstring.size() >= 3 && vstring[0] == "Company")
				{
					stringstream(vstring[2])>>temp_int[0];		// Company number
					if (vstring.size() == 9)
						stringstream(vstring[8])>>temp_int[1];		// Company type
					if (vstring.size() == 12)
						stringstream(vstring[11])>>temp_int[2];		// Nr of employees
					if (vstring.size() == 15)
						stringstream(vstring[14])>>temp_double[0];		// Balance
					if (vstring.size() == 18)
						stringstream(vstring[17])>>temp_int[3];		// Nr of goods
				}
				if ( vstring.size() >= 19 && vstring[18] == "==========")
				{
					company temp_comp(vstring[5], temp_int[0], temp_int[1], temp_int[2], temp_int [3], temp_double[0]);		// vstring[5] = company's name
					comp.push_back(temp_comp);
					vstring.clear();
				}
			}
		}
	}
}

int Add_company(vector<company> &comp)
{
	company temp;
	int error = 0;
	error = temp.NEW();
	if ( error == 0)
	{
		comp.push_back(temp);
		return temp.Get_coNr();
	}
	else
		return 0;
}

int Existing_company(vector<company> &comp)
{
	string temp_str;
	int size = comp.size();
	int temp = 0;
	for (int i=0;i<size;i++)
	{
		cout<<i+1<<". "<<comp[i].Get_name();
		if ( comp[i].Get_type() == 3)
			cout<<" Inc.";
		cout<<endl;
	}
	cout<<"0. Back"<<endl
		<<"Choose: ";
	getline(cin, temp_str);
	stringstream(temp_str)>>temp;
	if ( temp == 0)
		return 0;
	else if ( temp <= size && temp > 0)
		return temp;
	else
		return 0;
}

void start(vector<company> comp)
{
	string temp_str;
	int coNr = 0;
	int temp = 0;
	while (temp != 3)
	{
		system("CLS");
		cout<<"[1] - New Company"<<endl
			<<"[2] - Existing Company"<<endl
			<<"[3] - Exit"<<endl
			<<"Choose: ";
		getline(cin, temp_str);
		stringstream(temp_str)>>temp;
		if ( temp == 1)
		{
			coNr = Add_company(comp);
		}
		else if ( temp == 2)
		{
			Init(comp);
			coNr = Existing_company(comp);
		}
		if ( coNr > 0 && (temp == 1 || temp == 2) )
		{
			comp[coNr-1].company_Menu();
			coNr = 0;
		}
	}
}

int main(void)
{
	setlocale(LC_ALL, "swedish");
	srand(time(0));
	vector<company> comp;

	start(comp);
	return 0;
}