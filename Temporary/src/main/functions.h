#ifndef FUNC_H
#define FUNC_H

#include "stdAfx.h"

void explode(std::string str, std::string arg, std::vector<std::string> * result);
int getFileSize(std::string fileName);

#endif