#ifndef FORDON_H
#define FORDON_H

class fordon
{
	int type;
	int year;
	double price;
	std::string owner;
	std::string brand;
public:
	fordon(void);
	~fordon(void);
	void Set_type(int t_type);
	void Set_year(void);
	void Set_year(int temp_year);
	void Set_owner(void);
	void Set_owner(std::string temp_owner);
	void Set_brand(void);
	void Set_brand(std::string temp_brand);
	void Set_price(void);
	void Set_price(double temp_price);
	std::string get_brand();
	std::string get_owner();
	int get_year();
	double get_price();
	int get_type();
};

#endif