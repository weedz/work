#ifndef CYKEL_H
#define CYKEL_H

#include "stdAfx.h"

class cykel :
	public fordon
{
	int sizeInch;
	int model;
public:
	cykel(void);
	~cykel(void);
	void Set_sizeInch(int temp_size);
	void Set_model(int t_model);
	int get_sizeInch();
	int get_model();
};

class tvhjul :
	public cykel
{
	int flak;
public:
	tvhjul(void);
	~tvhjul(void);
	void show(void);
	void Make(void);
	void Make(std::string t_manu, int t_year, int t_model, int t_size, int t_flak, std::string t_owner, double t_price);
	void write_ToFile(int compNr);
};

class trhjul :
	public cykel
{
public:
	int gears;
	trhjul(void);
	~trhjul(void);
	void show(void);
	void Make(void);
	void Make(std::string t_manu, int t_year, int t_model, int t_size, int t_gears, std::string t_owner, double t_price);
	void write_ToFile(int compNr);
};

#endif