#include "stdAfx.h"
using namespace std;
bil::bil(void)
{
}
bil::~bil(void)
{
}

void bil::Set_model(string temp_style)
{
}
string bil::get_model(void)
{
	return model;
}

personbil::personbil(void)
{
}
personbil::~personbil(void)
{
}

lastbil::lastbil(void)
{
}
lastbil::~lastbil(void)
{
}

void lastbil::Make(void)
{
	bil::fordon::Set_type(2);
	int randbil = rand()%3;
	if ( randbil == 0)
	{
		bil::Set_brand("Volvo");
		bil::Set_owner("Carl-Bildt");
		bil::Set_year(1988);
		bil::Set_model("");
		bil::Set_price(49990);
		weight = 6.2;
	}
	else if ( randbil == 1)
	{
		bil::Set_brand("Ford");
		bil::Set_owner("Kalle_Lund");
		bil::Set_year(2012);
		bil::Set_model("");
		bil::Set_price(999000);
		weight = 8.4;
	}
	else if ( randbil == 2)
	{
		bil::Set_brand("Chrysler");
		bil::Set_owner("Kungen");
		bil::Set_year(1995);
		bil::Set_model("");
		bil::Set_price(84950);
		weight = 6.7;
	}
	bil::Set_model("1");
}

void lastbil::Make(string t_manu, int t_year, string t_model, double t_weight, string t_owner, double t_price)
{
	bil::Set_brand(t_manu);
	bil::Set_year(t_year);
	bil::Set_model(t_model);
	bil::Set_owner(t_owner);
	bil::Set_price(t_price);
	weight = t_weight;
}

void lastbil::show(void)
{
	cout<<"Manufacturer: "<<bil::get_brand()<<endl;
	cout<<"Year: "<<bil::get_year()<<endl;
	cout<<"Model: "<<bil::get_model()<<endl;
	cout<<"Weight: "<<weight<<endl;
	cout<<"Owner: "<<bil::get_owner()<<endl;
	cout<<"Price: "<<bil::get_price()<<endl;
}

void personbil::Make(void)
{
	bil::fordon::Set_type(1);
	int randbil = rand()%3;
	if ( randbil == 0)
	{
		bil::Set_brand("Toyota");
		bil::Set_owner("Carl-Bildt");
		bil::Set_year(1998);
		bil::Set_model("");
		bil::Set_price(11295);
		seats = 5;
	}
	else if ( randbil == 1)
	{
		bil::Set_brand("Fiat");
		bil::Set_owner("Kalle_Lund");
		bil::Set_year(2002);
		bil::Set_model("");
		bil::Set_price(15950);
		seats = 7;
	}
	else if ( randbil == 2)
	{
		bil::Set_brand("Volvo");
		bil::Set_owner("Kungen");
		bil::Set_year(2012);
		bil::Set_model("");
		bil::Set_price(389450);
		seats = 5;
	}
	bil::Set_model("1");
}

void personbil::Make(string t_manu, int t_year, string t_model, int t_seats, string t_owner, double t_price)
{
	bil::Set_brand(t_manu);
	bil::Set_year(t_year);
	bil::Set_model(t_model);
	bil::Set_owner(t_owner);
	bil::Set_price(t_price);
	seats = t_seats;
}

void personbil::show(void)
{
	cout<<"Manufacturer: "<<bil::get_brand()<<endl;
	cout<<"Year: "<<bil::get_year()<<endl;
	cout<<"Model: "<<bil::get_model()<<endl;
	cout<<"Seats: "<<seats<<endl;
	cout<<"Owner: "<<bil::get_owner()<<endl;
	cout<<"Price: "<<bil::get_price()<<endl;
}

void lastbil::write_ToFile(int compNr)
{
	fstream file(goodsFile, ios::app);
	file<<"Company = "<<compNr<<endl
		<<"Type = "<<bil::get_type()<<endl
		<<"Manufacturer = "<<bil::get_brand()<<endl
		<<"Year = "<<bil::get_year()<<endl
		<<"Model = "<<bil::get_model()<<endl
		<<"Weight = "<<weight<<endl
		<<"Owner = "<<bil::get_owner()<<endl
		<<"Price = "<<bil::get_price()<<endl
		<<setfill('=')<<setw(10)<<"="<<endl;
}

void personbil::write_ToFile(int compNr)
{
	fstream file(goodsFile, ios::app);
	file<<"Company = "<<compNr<<endl
		<<"Type = "<<bil::get_type()<<endl
		<<"Manufacturer = "<<bil::get_brand()<<endl
		<<"Year = "<<bil::get_year()<<endl
		<<"Model = "<<bil::get_model()<<endl
		<<"Seats = "<<seats<<endl
		<<"Owner = "<<bil::get_owner()<<endl
		<<"Price = "<<bil::get_price()<<endl
		<<setfill('=')<<setw(10)<<"="<<endl;
}