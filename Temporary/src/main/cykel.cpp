#include "stdAfx.h"
using namespace std;
cykel::cykel(void)
{
}
cykel::~cykel(void)
{
}

void cykel::Set_sizeInch(int temp_size)
{
	sizeInch = temp_size;
}

void cykel::Set_model(int t_model)
{
	model = t_model;
}

int cykel::get_sizeInch()
{
	return sizeInch;
}

int cykel::get_model()
{
	return model;
}

tvhjul::tvhjul(void)
{
}
tvhjul::~tvhjul(void)
{
}

trhjul::trhjul(void)
{
}
trhjul::~trhjul(void)
{
}

void tvhjul::show(void)
{
	cout<<"Manufacturer: "<<cykel::get_brand()<<endl;
	cout<<"Year: "<<cykel::get_year()<<endl;
	cout<<"Size: "<<cykel::get_sizeInch()<<"\""<<endl;
	cout<<"Model: "<<cykel::get_model()<<endl;
	cout<<"Flak: "<<flak<<endl;
	cout<<"Owner: "<<cykel::get_owner()<<endl;
	cout<<"Price: "<<cykel::get_price()<<endl;
}

void tvhjul::Make(void)
{
	cykel::fordon::Set_type(4);
	int randcykel = rand()%3;
	if ( randcykel == 0)
	{
		cykel::Set_sizeInch(21);
		cykel::Set_brand("Kronan");
		cykel::Set_owner("Carl-Bildt");
		cykel::Set_year(1998);
		cykel::Set_model(1);
		cykel::Set_price(1195);
		flak = 1;
	}
	else if ( randcykel == 1)
	{
		cykel::Set_sizeInch(12);
		cykel::Set_brand("Kronan");
		cykel::Set_owner("Kalle_Lund");
		cykel::Set_year(2002);
		cykel::Set_model(1);
		cykel::Set_price(895);
		flak = 1;
	}
	else if ( randcykel == 2)
	{
		cykel::Set_sizeInch(22);
		cykel::Set_brand("Kronan");
		cykel::Set_owner("Kungen");
		cykel::Set_year(2004);
		cykel::Set_model(1);
		cykel::Set_price(1495);
		flak = 0;
	}
}

void tvhjul::Make(string t_manu, int t_year, int t_model, int t_size, int t_flak, string t_owner, double t_price)
{
	cykel::Set_brand(t_manu);
	cykel::Set_year(t_year);
	cykel::Set_model(t_model);
	cykel::Set_sizeInch(t_size);
	cykel::Set_owner(t_owner);
	cykel::Set_price(t_price);
	flak = t_flak;
}

void trhjul::show(void)
{
	cout<<"Manufacturer: "<<cykel::get_brand()<<endl;
	cout<<"Year: "<<cykel::get_year()<<endl;
	cout<<"Size: "<<cykel::get_sizeInch()<<"\""<<endl;
	cout<<"Model: "<<cykel::get_model()<<endl;
	cout<<"Gears: "<<gears<<endl;
	cout<<"Owner: "<<cykel::get_owner()<<endl;
	cout<<"Price: "<<cykel::get_price()<<endl;
}

void trhjul::Make(void)
{
	cykel::fordon::Set_type(3);
	int randcykel = rand()%3;
	if ( randcykel == 0)
	{
		cykel::Set_sizeInch(21);
		cykel::Set_brand("Kronan");
		cykel::Set_owner("Carl-Bildt");
		cykel::Set_year(1998);
		cykel::Set_model(1);
		cykel::Set_price(1895);
		gears = 24;
	}
	else if ( randcykel == 1)
	{
		cykel::Set_sizeInch(12);
		cykel::Set_brand("Kronan");
		cykel::Set_owner("Kalle_Lund");
		cykel::Set_year(2002);
		cykel::Set_model(1);
		cykel::Set_price(1295);
		gears = 3;
	}
	else if ( randcykel == 2)
	{
		cykel::Set_sizeInch(22);
		cykel::Set_brand("Kronan");
		cykel::Set_owner("Kungen");
		cykel::Set_year(2004);
		cykel::Set_model(1);
		cykel::Set_price(2249);
		gears = 1;
	}
}

void trhjul::Make(string t_manu, int t_year, int t_model, int t_size, int t_gears, string t_owner, double t_price)
{
	cykel::Set_brand(t_manu);
	cykel::Set_year(t_year);
	cykel::Set_model(t_model);
	cykel::Set_sizeInch(t_size);
	cykel::Set_owner(t_owner);
	cykel::Set_price(t_price);
	gears = t_gears;
}

void tvhjul::write_ToFile(int compNr)
{
	fstream file(goodsFile, ios::app);
	file<<"Company = "<<compNr<<endl
		<<"Type = "<<cykel::get_type()<<endl
		<<"Manufacturer = "<<cykel::get_brand()<<endl
		<<"Year = "<<cykel::get_year()<<endl
		<<"Model = "<<cykel::get_model()<<endl
		<<"Size = "<<cykel::get_sizeInch()<<endl
		<<"Flak = "<<flak<<endl
		<<"Owner = "<<cykel::get_owner()<<endl
		<<"Price = "<<cykel::get_price()<<endl
		<<setfill('=')<<setw(10)<<"="<<endl;
}

void trhjul::write_ToFile(int compNr)
{
	fstream file(goodsFile, ios::app);
	file<<"Company = "<<compNr<<endl
		<<"Type = "<<cykel::get_type()<<endl
		<<"Manufacturer = "<<cykel::get_brand()<<endl
		<<"Year = "<<cykel::get_year()<<endl
		<<"Model = "<<cykel::get_model()<<endl
		<<"Size = "<<cykel::get_sizeInch()<<endl
		<<"Gears = "<<gears<<endl
		<<"Owner = "<<cykel::get_owner()<<endl
		<<"Price = "<<cykel::get_price()<<endl
		<<setfill('=')<<setw(10)<<"="<<endl;
}