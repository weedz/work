#ifndef __STDAFX_H
#define __STDAFX_H

#define LANGUAGE_US
#undef LANGUAGE_SV

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>
#include <ctime>
#include <clocale>

const std::string companyFile = "company.txt";
const std::string goodsFile = "goods.txt";

#include "language.h"
#include "functions.h"

#include "fordon.h"
#include "bil.h"
#include "cykel.h"
#include "company.h"
#include "seller.h"

#endif