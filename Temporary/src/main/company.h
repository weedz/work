#ifndef COMPANY_H
#define COMPANY_H

#include "stdAfx.h"

class company
{
	std::vector<personbil> pbil;
	std::vector<lastbil> lbil;
	std::vector<tvhjul> tvcykel;
	std::vector<trhjul> trcykel;

	int type;
	int coNr;
	double balance;
	int nrEmployees;
	int goods;
	std::string name;
public:
	company(void);
	~company(void);
	company(std::string temp_name, int temp_coNr, int temp_type, int temp_nrEmployees, int temp_goods, double temp_balance);

	int NEW(void);
	int CheckStatus(void);
	void Set_goods(void);
	void Init_goods(void);
	void list_goods(void);

	std::string Set_name(void);
	void Set_name(std::string temp_str);
	int Set_type(void);
	void Set_type(int temp_int);
	int Set_coNr(void);
	void Set_coNr(int temp_coNr);
	void Set_balance(int temp_int);
	void Set_nr_goods(int temp_int);
	void add_goods(void);
	void add_goods(int type, int model);

	int CheckFile(void);
	void writeToFile(void);
	void Update_nrEmployees(void);
	void Update_goods(void);
	int company_Menu(void);

	std::string Get_typeString(void);
	int Get_type(void);
	int Get_coNr(void);
	std::string Get_name(void);

	void sell_menu01(void);
	void sell_menu02(void);
};

#endif