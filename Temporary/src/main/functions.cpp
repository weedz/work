#include "stdAfx.h"
using namespace std;
/* Function used to split a string where "str LIKE arg". splits str to result.
Usage:
----------------------------------------------
string temp = "This is a sentence";
vector<string> result;
explode(temp, " ", result);
for (int i=0;i<result.size();i++)
{
	cout<<result[i]<<endl;
}
----------------------------------------------
Gives this output:
This
is
a
sentence
*/
void explode(string str, string arg, vector<std::string> * result)
{
	int found;
	found = str.find_first_of(arg);
	while( found != std::string::npos)
	{
		if ( found > 0)
		{
			result->push_back(str.substr(0, found));
		}
		str = str.substr(found+1);
		found = str.find_first_of(arg);
	}
	if ( str.length() > 0)
	{
		result->push_back(str);
	}
}

int getFileSize(string fileName)
{
	long begin = 0, end = 0;
	ifstream file(fileName);
	if ( file.is_open() && file.good())
	{
		begin = file.tellg();
		file.seekg(0, ios::end);
		end = file.tellg();
	}
	return end-begin;
}