#ifndef BIL_H
#define BIL_H

#include "stdAfx.h"

class bil :
	public fordon
{
	int regNr;
	std::string model;
public:
	bil(void);
	~bil(void);
	void Set_model(std::string temp_style);
	std::string get_model(void);
};

class personbil :
	public bil
{
	int seats;
public:
	personbil(void);
	~personbil(void);
	void show(void);
	void Make(void);
	void Make(std::string t_manu, int t_year, std::string t_model, int t_seats, std::string t_owner, double t_price);
	void write_ToFile(int compNr);
};

class lastbil :
	public bil
{
	double weight;
public:
	lastbil(void);
	~lastbil(void);
	void show(void);
	void Make(void);
	void Make(std::string t_manu, int t_year, std::string t_model, double t_weight, std::string t_owner, double t_price);
	void write_ToFile(int compNr);
};

#endif