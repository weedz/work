#ifdef LANGUAGE_US
#define _COMP_TYPE_1 "Sole Trader"
#define _COMP_TYPE_2 "Trading Company"
#define _COMP_TYPE_3 "Incorporated Company"
#define _COMP_MENU "Company Menu"
#endif

#ifdef LANGUAGE_SV
#define _COMP_TYPE_1 "Enskilt Företag"
#define _COMP_TYPE_2 "Handelsföretag"
#define _COMP_TYPE_3 "Aktiebolag"
#define _COMP_MENU "Företags Meny"
#endif